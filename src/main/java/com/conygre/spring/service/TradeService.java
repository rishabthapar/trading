package com.conygre.spring.service;

import java.util.Collection;
import java.util.List;

import com.conygre.spring.entities.Trade;

import org.bson.types.ObjectId;

public interface TradeService{
    void addTrade(Trade trade);
    Collection<Trade> getAllTrades();
    void removeTrade(ObjectId tradeId);
    void updateTradeStatus(ObjectId tradeId, String status);
    Trade getTradebyId(ObjectId tradeID);
    List<Trade> getTradeByTicker(String ticker);

}