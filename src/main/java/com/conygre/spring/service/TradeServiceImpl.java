package com.conygre.spring.service;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.conygre.spring.data.TradeRepo;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeStatus;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;


@Service
public class TradeServiceImpl implements TradeService {
    private Collection<Trade> tradeList;

    @Autowired
    private TradeRepo repo;

    @Override
    public void addTrade(Trade trade) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date now = new Date();
        trade.setDateCreated(formatter.format(now));  
        repo.insert(trade);

    }

    @Override
    public Collection<Trade> getAllTrades() {
        tradeList = repo.findAll();
        Collections.unmodifiableCollection(tradeList);
        return tradeList;
    }

    @Override
    public void removeTrade(ObjectId tradeId) throws HttpClientErrorException{
        
        if (repo.findById(tradeId).isPresent()) {
            repo.deleteById(tradeId);
        }
        else{
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public void updateTradeStatus(ObjectId tradeId, String status) throws HttpClientErrorException {
        
        Optional<Trade> t = repo.findById(tradeId);
        TradeStatus s = TradeStatus.valueOf(status);
        if(t.isPresent()) {
            if (t.get().getStatus() == TradeStatus.CREATED){
            Trade t1 = t.get();
            t1.setStatus(s);
            repo.save(t1); 
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE);
            }
        }else {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND); 
            }
    }
    @Override
    public Trade getTradebyId(ObjectId tradeID) throws HttpClientErrorException {
        Optional<Trade> trade = repo.findById(tradeID);
        if (!trade.isPresent()){
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
        return trade.get();

    }

    @Override
    public List<Trade> getTradeByTicker(String ticker) {
        // Query query = new Query();
        // query.addCriteria(Criteria.where("ticker").is(ticker));
        // Collection<Trade> trades = repo.find(query, Trade.class);
        List<Trade> foundTrades = repo.findByTicker(ticker);
        if (foundTrades.isEmpty()) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        } else {
            return foundTrades;
        }
        
    }
    
}