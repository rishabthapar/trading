package com.conygre.spring.controller;

import com.fasterxml.jackson.databind.util.StdConverter;

import org.bson.types.ObjectId;

public class StringToID extends StdConverter<String,ObjectId> {
        
    @Override
		public ObjectId convert(String ID) {
				return new ObjectId(ID);
		}
}