package com.conygre.spring.controller;

import com.fasterxml.jackson.databind.util.StdConverter;

import org.bson.types.ObjectId;

public class IDToString extends StdConverter<ObjectId,String> {
    @Override
    public String convert(ObjectId ID) {
            return ID.toString();
    }
}
