package com.conygre.spring.controller;

import java.util.Collection;
import java.util.List;

import com.conygre.spring.entities.Trade;
import com.conygre.spring.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/trades")
public class TradeController {
    @Autowired
    private TradeService tService;

    @RequestMapping(method=RequestMethod.POST)
    public void addTrade(@RequestBody Trade trade) {
        tService.addTrade(trade);
    }
    
    @RequestMapping(method=RequestMethod.GET, value = "/id/{id}")
    public Trade getTradebyId(@PathVariable("id") String tradeId) throws HttpClientErrorException{
        return tService.getTradebyId(new ObjectId("" + tradeId));
    }

    @RequestMapping(method=RequestMethod.PATCH, value = "/update/{id}" )
    public void updateTradeStatus(@PathVariable("id") String tradeId, @RequestBody String status) throws HttpClientErrorException{
        tService.updateTradeStatus(new ObjectId("" + tradeId), status);
    }

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Trade> getTrade(){
        return tService.getAllTrades();
    }

    @RequestMapping(method=RequestMethod.DELETE, value = "/{id}")
    public void deleteTradebyId(@PathVariable("id") String tradeID) throws HttpClientErrorException{
        tService.removeTrade(new ObjectId(""+tradeID));
    }

    @RequestMapping(method=RequestMethod.GET, value = "/ticker/{ticker}")
    public List<Trade> getbyTicker(@PathVariable("ticker") String ticker) throws HttpClientErrorException {
        return tService.getTradeByTicker(ticker);
    }
    
}