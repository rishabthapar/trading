package com.conygre.spring.entities;

public enum TradeStatus {
    
    CREATED("CREATED"), PENDING("PENDING"), 
    CANCELLED("CANCELLED"), REJECTED("REJECTED"),
    FILLED("FILLED"), PARTIALLY_FILLED("PARTIALLY_FILLED"), 
    ERROR("ERROR");

    private String state;

    private TradeStatus(String state){
        this.state = state;
    }

    public String getState(){
        return this.state;
    }

}