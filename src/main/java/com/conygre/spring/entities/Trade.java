package com.conygre.spring.entities;


import com.conygre.spring.controller.IDToString;
import com.conygre.spring.controller.StringToID;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    @JsonSerialize(converter = IDToString.class)
    @JsonDeserialize(converter = StringToID.class)
    private ObjectId id;
    private String dateCreated;
    private String ticker;
    private double quantity;
    private double price;
    private TradeStatus status;
    private TradeType type;
    

    public Trade(String ticker, double quantity, double price, TradeStatus status, TradeType type){
        this.dateCreated = "";
        this.ticker = ticker;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
        this.type = type;
    }

    public Trade(){
        
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    
}

