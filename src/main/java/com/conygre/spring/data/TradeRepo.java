package com.conygre.spring.data;

import java.util.Collection;
import java.util.List;

import com.conygre.spring.entities.Trade;

import org.bson.types.ObjectId;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepo extends MongoRepository<Trade, ObjectId> {
    public List<Trade> findByTicker(String ticker);
}