package com.conygre.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories("com.conygre.spring.data")
public class TradingHackathonApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TradingHackathonApplication.class, args);
	}

}
