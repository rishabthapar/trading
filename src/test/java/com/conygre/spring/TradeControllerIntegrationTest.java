package com.conygre.spring;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.ArgumentMatchers.isNull;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.TradingHackathonApplication;
import com.conygre.spring.controller.TradeController;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeStatus;
import com.conygre.spring.entities.TradeType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import org.bson.types.ObjectId;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TradingHackathonApplication.class)
public class TradeControllerIntegrationTest {

    public static final String TEST_ID = "5f63b7b83f9e2449f4b44751";
    

    @Autowired
    private TradeController controller;

    @Test
    public void testAddTrade(){
        Trade trade = new Trade("test", 0, 0, TradeStatus.CREATED, TradeType.BUY);
        controller.addTrade(trade);
        Iterable<Trade> trades = controller.getTrade();
        Stream<Trade> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    

    @Test
    public void testGetTrade() {
        Iterable<Trade> trades = controller.getTrade();
        Stream<Trade> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L)); 
    }

   @Test
    public void testGetTradeById() {
        Trade trade = controller.getTradebyId(TEST_ID);
        assertNotNull(trade);
    } 


    @Test
    public void testUpdateTradeStatus() {
        controller.updateTradeStatus(TEST_ID, "CANCELLED");
        Trade trade = controller.getTradebyId(TEST_ID);
        //TradeStatus CANCELLED = TradeStatus.CANCELLED;
        assertThat(trade.getStatus(), equalTo(TradeStatus.CANCELLED));
    }

    @Test
    public void testGetTradeByTicker(){
        Trade newTrade = new Trade("test", 9, 9, TradeStatus.CREATED, TradeType.BUY);
        controller.addTrade(newTrade);
        List<Trade> trade = controller.getbyTicker("test");
        assertThat(trade.size(), equalTo(2));
        

    }

    @Test
    public void testDeleteById(){
        controller.deleteTradebyId(TEST_ID);
        
        Exception exception = assertThrows(Exception.class, () -> controller.getTradebyId(TEST_ID));
        assertEquals("404 NOT_FOUND", exception.getMessage());
    }

    @Test
    public void testGetTradeByFakeId() {
        Exception exception = assertThrows(Exception.class, () -> controller.getTradebyId(TEST_ID));
        assertEquals("404 NOT_FOUND", exception.getMessage());
    } 

    @Test
    public void testDeleteByFakeId(){
        Exception exception = assertThrows(Exception.class, () -> controller.deleteTradebyId(TEST_ID));
        assertEquals("404 NOT_FOUND", exception.getMessage());
    }

    
}
