package com.conygre.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ch.qos.logback.core.subst.Token.Type;

import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.controller.TradeController;
import com.conygre.spring.data.TradeRepo;
import com.conygre.spring.service.TradeService;
import com.conygre.spring.entities.TradeStatus;
import com.conygre.spring.entities.TradeType;
import com.conygre.spring.entities.Trade;

import org.bson.types.ObjectId;



@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TradeControllerUnitTest.Config.class)
public class TradeControllerUnitTest {

    public static final String TEST_ID = "5f4d2462fd24d34aa86307f7";
    ObjectId _id = new ObjectId(TEST_ID);

    @Configuration
    public static class Config {

        @Bean
        public TradeRepo repo(){
            return mock(TradeRepo.class);
        }

        @Bean
        public TradeService service() {
            ObjectId _id = new ObjectId(TEST_ID);
            TradeStatus status = TradeStatus.CREATED;
            TradeType type = TradeType.BUY;
            Trade trade = new Trade("", 0, 0, status, type);
            ArrayList<Trade> trades = new ArrayList<>();
            trades.add(trade);

            TradeService service = mock(TradeService.class);
            when(service.getAllTrades()).thenReturn(trades);
            when(service.getTradebyId(_id)).thenReturn(trade);
           
            
            return service;
        }

        @Bean 
        public TradeController controller(){
            return new TradeController();
        }

    }

    @Autowired
    private TradeController controller;

    @Test
    public void testGetTrade() {
        Iterable<Trade> trades = controller.getTrade();
        Stream<Trade> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
       
    }

   @Test
    public void testGetTradeById() {
        Trade trade = controller.getTradebyId(TEST_ID);
        assertNotNull(trade);
    }
} 
